#@METADATASTART
#@DetailDescriptionStart
##############################################################################
#
# Note: Creates a import file for mRemoteNG
#
# Softwarename : mRemoteImport.pl
# Version : 2.1
#
# Purpose: Add a single address to a selection of networks
# Copyright (c) 2019 BELL Computer-Netzwerke GmbH
# Copyright (c) 2019 Stephan Harrer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
##############################################################################
#@DetailDescriptionEnd
#
#@SectionStart ( description = "Fill in the import filename. This name is used as foldername in mRemote")
#
#@VariableFieldLabel (
#          description   = "Import filename",
#          type          = String,
#          scope         = global,
#          required      = yes,
#          name          = "importFileName",
#          value         = "xmcImport"
#    )
#
#@SectionEnd
#@SectionStart ( description = "Do you want to use IP as devicename in mRemote? If you select no the systemname is used.")
#
#@VariableFieldLabel (
#          description   = "Use IP as devicename in mRemote",
#          type          = String,
#          scope         = global,
#          required      = yes,
#          validValues   = [yes, no],
#          value         = yes,
#          name          = "useIpAsName"
#    )
#
#@SectionEnd
#@MetaDataEnd


def main():

       
    #Create config file
    mremote = open('/tmp/' + emc_vars["importFileName"] + '.csv', 'w')
    

    #Write first line of config file
    mremote.write("Name;Parent;Icon;Panel;Username;Password;Hostname;Protocol;Port\n")
    
    nbiQuery = "{administration{profiles{profileName authCred {type userName loginPassword}}}}"
    
    result = emc_nbi.query(nbiQuery) 
    
    profiles = {}
    
    for item in result['administration']['profiles']:
        profilevalues = []
        print item['profileName'] + "  " + str(item['authCred']['type']) + " " + str(item['authCred']['userName']) + " " + str(item['authCred']['loginPassword'])
        profilevalues.append(str(item['authCred']['type']))
        profilevalues.append(str(item['authCred']['userName']))
        profilevalues.append(str(item['authCred']['loginPassword']))
        
        profiles[item['profileName']] = profilevalues
        
    
    for k, v in profiles.iteritems():
        print k, v
        
        
    nbiQuery = "{network{devices{ip deviceData { sysName nickname profileName }}}}"
    
    result = emc_nbi.query(nbiQuery)
    
    for item in result['network']['devices']:
        if item['deviceData']:
            print item['ip'] + "," + str(item['deviceData']['sysName']) + "," + str(item['deviceData']['nickname']) + "," + str(item['deviceData']['profileName'])
            
            if emc_vars["useIpAsName"] == "yes":
                devicename = item['ip']
            else:
                devicename = str(item['deviceData']['sysName']) if str(item['deviceData']['sysName']) else str(item['deviceData']['nickname'])
            
            protocol = profiles[str(item['deviceData']['profileName'])][0]
            
            if protocol == "SSH":
                symbol = "SSH"
                prot = "SSH2"
                port = "22"
            else:
                symbol = "Telnet"
                prot = "Telnet"
                port = "23" 
                
            username = profiles[str(item['deviceData']['profileName'])][1]
            password = profiles[str(item['deviceData']['profileName'])][2]
                   
            #Write config file    
            if protocol != "None" and username != "None":
                mremote.write(devicename + ";Home;" + symbol + ";Home;" + username + ";" + password + ";" + item['ip'] + ";" + prot + ";" + port + "\n")
        
    mremote.close()

main()