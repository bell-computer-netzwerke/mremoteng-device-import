# mRemoteNG Device Import

Little program to create a mRemoteNG import file for all Extreme Management (XMC) devices with username and password.

## How to use

You can run the script [mRemoteImport.py](mRemoteImport.py) directly in the XMC menu _Tasks->Scripts_

![Tasks](xmc.jpg)


*   When you start the script select an arbitrary device
*   Fill in a file name for the config file. That name is used as folder in mRemoteImport
*   Select if you want to use the device name or IP in mRemote

![Options](option.jpg)

*	After the program ran, the config file named with the selected name will be created in the folder _/tmp_ as _yourfilename.csv_
*	You can copy the file with WinSCP to you computer for example
*   In mRemoteNG go to _File->Import->Import from File_

![mRemoteImport](mRemoteNG.jpg)